

#
# This script updates the assignment2 gitlab repository. 
# 


# remove program created files and directories (if they exist)
rm -rf ./run_files
rm -rf ./paragraphs
# don't do this, in case they don't have pytrec_eval installed
#(leave the results file)
#rm -rf trec_eval_results.txt


# add changes in local repo
git add .

# if commit message given as (1 and only 1) arg, use it
# otherwise open gedit through the terminal for message
if [ $# -eq 0 ]
  then
    echo "No arguments supplied, opening terminal text editor"
    git commit
elif [ "$#" -ne 1 ];
    then
    echo "Illegal number of parameters: $#"
    exit 1
else
    git commit  -m '$1'
fi

# push changes after commit
git push origin

git push origin HEAD:master --force
