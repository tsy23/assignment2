#!/bin/bash
#
# Compile the program (and initiate program files and directories)
#
#


# remove program created files and directories (if they exist)
rm -rf ./run_files
rm -rf ./paragraphs
rm -rf ./plots
# don't do this, in case they don't have pytrec_eval installed
#(leave the results file)
#rm -rf trec_eval_results.txt
rm -rf query-arith-mean-and-stderr.txt

# maven compile command
mvn clean compile assembly:single
if [ $? -eq 1 ]; then
	echo "Maven compile failed"
	exit 1
fi

# create a run_files directory
mkdir ./run_files
mkdir ./plots
