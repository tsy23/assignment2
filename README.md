# Assignment2

CS853 Assignment2

There is a script to run all parts of this program and it is

bash prog2.sh

The manual steps for running the program are 

compile.sh

java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar part1
       
python3 part2.py

java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar parts3-5


The first 2 commands run part 1 which sets up the index and creates run files.
The python command runs part 2 which uses trec_eval on the given measures.
The final command runs parts 3-5 which uses the searcher and evaluation
algorithms to compare to the trec_eval results.


I am unsure if my calculations are correct, the differences are currently
somewhat reasonable and am unsure of how close to the qrel results they should be.
