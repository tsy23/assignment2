
/**
 * LuceneSearcher.java
 * 
 * UNH CS753
 * Progrmming Assignment 2
 * Group 2
 * 
 * This class is responsible for the bulk of the work in this program.
 * After index creation, it is called upon repeatedly to perform searches
 * and evaluation routines per program 2 specifications. 
 * 
**/

package edu.unh.cs753.indexing;

import edu.unh.cs753.utils.SearchUtils;
import edu.unh.cs753.utils.IndexUtils;
import edu.unh.cs753.utils.QrelMap;
import edu.unh.cs.treccar_v2.Data;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.similarities.BasicStats;
import org.apache.lucene.search.similarities.SimilarityBase;
import org.apache.lucene.search.MatchAllDocsQuery;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.text.DecimalFormat;
import org.apache.lucene.search.similarities.BM25Similarity;



public class LuceneSearcher { 
	
    public final IndexSearcher searcher;
    private String methodName;
    private static final int MAX_REL_DOCS = Integer.MAX_VALUE;
    private final static int WIDTH = 1000, HEIGHT = 750;
    
    
	/** 
	 * Construct a Lucene Searcher.
	 * @param indexLoc: the path containing the index.
    */
    public LuceneSearcher(String indexLoc) {
        searcher = SearchUtils.createIndexSearcher(indexLoc);
        methodName = "default";
        searcher.setSimilarity(new BM25Similarity());
    }

    /**
     * Function: query
     * Desc: Queries Lucene paragraph corpus using a standard similarity function.
     *       Note that this uses the StandardAnalyzer.
     * @param queryString: The query string that will be turned into a boolean query.
     * @param nResults: How many search results should be returned
     * @return TopDocs (ranked results matching query)
     */
    public TopDocs query(String queryString, Integer nResults) {
        Query q = SearchUtils.createStandardBooleanQuery(queryString, "text");
        try {
            return searcher.search(q, nResults);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


	/**
     * Function: custom
     * Desc: A custom scoring function which is the sum of hits within a document.
     */
    public void custom() throws IOException {
        methodName = "custom";
        SimilarityBase mysimilarity= new SimilarityBase() {
            @Override
            protected float score(BasicStats basicStats, float v, float v1) {
                float sum1 = 0.0f;
                sum1 += v;
                return sum1;
            }

            @Override
            public String toString() {
                return null;
            }
        };
        searcher.setSimilarity(mysimilarity);
    }
    
    
    /**
     * Function: getRPrec
     * Desc: Return the R precision (average among all queries).
     * @param paraFilePath: The path where the paragraph file exists.
     * @param pageFilePath: The path where the page file exists.
     * @param qrelFilePath: The path where the qrel file exists.
     * @return average result among queries.
     */
    public double getRPrec(String paraFilePath, String pageFilePath,
                           String qrelFilePath) throws IOException {
                               
        QrelMap qMap = new QrelMap(qrelFilePath);
        double total = 0;
        final int nQueries = getQueries(pageFilePath).size();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            int hits = 0;
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            int R = topDocs.scoreDocs.length;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                if(qMap.isRelevant(queryId, paraId)) { 
                    hits++;
                }
            }
            if(R > 0) 
                total += (double)hits / R;
        }
        return (double) total/nQueries;
    }
    
    
    /**
     * Function: plotRPrecStdErr
     * Desc: Plot R precision standard error results.
     * @param paraFilePath: The path where the paragraph file exists.
     * @param pageFilePath: The path where the page file exists.
     * @param qrelFilePath: The path where the qrel file exists.
     * @return average result among queries.
     */
    public void plotRPrecStdErr(String paraFilePath, String pageFilePath,
                           String qrelFilePath) throws IOException {
                               
        QrelMap qMap = new QrelMap(qrelFilePath);
        double stdErrorTotal = 0, meanTotal = 0;
        final int nQueries = getQueries(pageFilePath).size();
        XYSeries series = new XYSeries("Data");
        int cnt = 0;
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            int hits = 0;
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            int R = topDocs.scoreDocs.length;
            double mean = 0.0;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                if(qMap.isRelevant(queryId, paraId)) { 
                    mean += sd.score;
                    hits++;
                }
            }
            mean /= R;
            meanTotal += mean;
            double tmpTotal = 0.0;
            double deviation = 0.0;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                if(qMap.isRelevant(queryId, paraId)) { 
                    double score = sd.score;
                    deviation = Math.abs(score - mean);
                    if(score > mean) 
                        deviation = -deviation;
                    deviation *= deviation;
                    tmpTotal += deviation;
                }
            }
            if(R > 1) {
                double tmp = (double)tmpTotal / (R-1);
                double stdDeviation = Math.sqrt(tmp);
                double stdError = (double)stdDeviation / Math.sqrt(R);
                stdErrorTotal += stdError;
                series.add(cnt++, stdError);
                
            }
        }
        final XYSeriesCollection data = new XYSeriesCollection(series);
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "R-Precision Standard Error: " + methodName,
            "Queries", 
            "Standard Error", 
            data,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(WIDTH, HEIGHT));
        meanTotal /= nQueries;
        stdErrorTotal /= nQueries;
        DecimalFormat df = new DecimalFormat("#.#");
        String tmp = df.format(stdErrorTotal);
        double value = Double.parseDouble(tmp);
        ValueMarker marker = new ValueMarker(value);  
        marker.setPaint(Color.black);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.addRangeMarker(marker);
        // Draw png
        BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = bi.getGraphics();
        chartPanel.setBounds(0, 0, WIDTH, HEIGHT);
        chartPanel.paint(graphics);
        ImageIO.write(bi, "png", new File("plots/R-Precision-Plot-Graph-" + methodName + ".png"));
        System.out.println("\tRPrec mean: " + meanTotal);
        System.out.println("\tRPrec standard Error: " + stdErrorTotal);
    }
    
    /**
     * Function: getMean
     * Desc: Return the Mean (map) (average among all queries).
     * @param paraFilePath: The path where the paragraph file exists.
     * @param pageFilePath: The path where the page file exists.
     * @param qrelFilePath: The path where the qrel file exists.
     * @return average result among queries.
     */
    public double getMeanPrec(String paraFilePath, String pageFilePath,
                          String qrelFilePath) throws IOException {
    	QrelMap qMap = new QrelMap(qrelFilePath);
        double total = 0;
        final int nQueries = getQueries(pageFilePath).size();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            double nQrelRelevantDocs = qMap.nRelevant(queryId);
            double curDocTotal = 0;
            long nRelevant = 0;
            int cnt = 0;
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                if(qMap.isRelevant(queryId, paraId)) {
                    nRelevant++;
                    curDocTotal += (double)nRelevant / (cnt+1);
                }
                cnt++;
            }
            if(nQrelRelevantDocs > 0) 
                total += (1/nQrelRelevantDocs) * curDocTotal;
        }
        return (double)total / nQueries;
    }
    
    
    public void plotMAPStdErr(String paraFilePath, String pageFilePath,
                          String qrelFilePath) throws IOException {
    	QrelMap qMap = new QrelMap(qrelFilePath);
        double stdErrorTotal = 0;
        double meanTotal = 0;
        final int nQueries = getQueries(pageFilePath).size();
        XYSeries series = new XYSeries("Data");
        int cnt2 = 0;
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            double nQrelRelevantDocs = qMap.nRelevant(queryId);
            long nRelevant = 0;
            int cnt = 0;
            double mean = 0.0;
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            int R = topDocs.scoreDocs.length;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                if(qMap.isRelevant(queryId, paraId)) {
                    nRelevant++;
                    meanTotal += (double)nRelevant / (cnt+1);
                    mean += (double)nRelevant / (cnt+1);
                }
                cnt++;
            }
            mean /= R;
            if(nQrelRelevantDocs > 0) {
                meanTotal = (1/nQrelRelevantDocs) * meanTotal;
                double tmpTotal = 0.0;
                double deviation = 0.0;
                for (ScoreDoc sd : topDocs.scoreDocs) { 
                    Document doc = searcher.doc(sd.doc);
                    String paraId = doc.get("id");
                    if(qMap.isRelevant(queryId, paraId)) { 
                        double score = sd.score;
                        deviation = Math.abs(score - mean);
                        if(score > mean) 
                            deviation = -deviation;
                        deviation *= deviation;
                        tmpTotal += deviation;
                    }
                }
                if(R <= 1) 
                    series.add(cnt2++, 0);
                else {
                    double tmp = (double)tmpTotal / (R-1);
                    double stdDeviation = Math.sqrt(tmp);
                    double stdError = (double)stdDeviation / Math.sqrt(R);
                    stdErrorTotal += stdError;
                    series.add(cnt2++, stdError);
                }
            }
        }
        final XYSeriesCollection data = new XYSeriesCollection(series);
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "MAP Standard Error: " + methodName,
            "Queries", 
            "Standard Error", 
            data,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(WIDTH, HEIGHT));
        stdErrorTotal /= nQueries;
        meanTotal /= nQueries;
        DecimalFormat df = new DecimalFormat("#.#");
        String tmp = df.format(stdErrorTotal);
        double value = Double.parseDouble(tmp);
        ValueMarker marker = new ValueMarker(value);  
        marker.setPaint(Color.black);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.addRangeMarker(marker);
        // Draw png
        BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = bi.getGraphics();
        chartPanel.setBounds(0, 0, WIDTH, HEIGHT);
        chartPanel.paint(graphics);
        ImageIO.write(bi, "png", new File("plots/MAP-Plot-Graph-" + methodName + ".png"));
        System.out.println("\tMAP mean: " + meanTotal);
        System.out.println("\tMAP standard Error: " + stdErrorTotal);
        
    }
    
    
    
    public double getNdcg20(String paraFilePath, String pageFilePath,
                            String qrelFilePath) throws IOException {
        QrelMap qMap = new QrelMap(qrelFilePath);
        double total = 0;
        int cnt = 0; 
        final int nQueries = getQueries(pageFilePath).size();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            double nQrelRelevantDocs = qMap.nRelevant(queryId);
            int i = 0;
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            int nRelevant = topDocs.scoreDocs.length;
            double idcg = 0, dcg = 0;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                int itemRelevance = 1;
                if(!qMap.isRelevant(queryId, paraId)) {
                    itemRelevance = 0;
                }
                idcg += (Math.pow(2, itemRelevance) - 1.0) * ( Math.log(2) / Math.log(i + 2) );
                i++;
            }
            i = 0;
            for (ScoreDoc sd : topDocs.scoreDocs) {
                if(i == 20)
                    break;
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                int itemRelevance = 1;
                if(!qMap.isRelevant(queryId, paraId))
                    itemRelevance = 0;
                int rank = i + 1;
                dcg += (Math.pow(2, itemRelevance) - 1.0) * (Math.log(2) / Math.log(rank + 1));
                i++;
            }
            if(idcg > 0) 
                total += (double) dcg / idcg;
        }
        return (double) total / nQueries;
    }
    
    
    
    public void plotNCDG20stdErr(String paraFilePath, String pageFilePath,
                            String qrelFilePath) throws IOException {
                                
        QrelMap qMap = new QrelMap(qrelFilePath);
        int cnt = 0, cnt2 = 0; 
        double stdErrorTotal = 0;
        double meanTotal = 0;
        XYSeries series = new XYSeries("Data");
        
        final int nQueries = getQueries(pageFilePath).size();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            double nQrelRelevantDocs = qMap.nRelevant(queryId);
            double cutoffAt20Total = 0;
            double tmpRes = 0;
            int i = 0;
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            int nRelevant = topDocs.scoreDocs.length;
            double idcg = 0, dcg = 0;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                int itemRelevance = 1;
                if(!qMap.isRelevant(queryId, paraId)) {
                    itemRelevance = 0;
                }
                idcg += (Math.pow(2, itemRelevance) - 1.0) * ( Math.log(2) / Math.log(i + 2) );
                i++;
            }
            i = 0;
            for (ScoreDoc sd : topDocs.scoreDocs) {
                if(i == 20)
                    break;
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                int itemRelevance = 1;
                if(!qMap.isRelevant(queryId, paraId))
                    itemRelevance = 0;
                int rank = i + 1;
                dcg += (Math.pow(2, itemRelevance) - 1.0) * (Math.log(2) / Math.log(rank + 1));
                i++;
            }
            if(idcg > 0) 
                tmpRes = (double) dcg / idcg;
            meanTotal += tmpRes;
            double tmpTotal = 0.0;
            double deviation = 0.0;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                if(qMap.isRelevant(queryId, paraId)) { 
                    double score = sd.score;
                    deviation = Math.abs(score - tmpRes);
                    if(score > tmpRes) 
                        deviation = -deviation;
                    deviation *= deviation;
                    tmpTotal += deviation;
                }
            }
            if(nRelevant <= 1) 
                series.add(cnt2++, 0);
            else {
                double tmp = (double)tmpTotal / (nRelevant-1);
                double stdDeviation = Math.sqrt(tmp);
                double stdError = (double)stdDeviation / Math.sqrt(nRelevant);
                stdErrorTotal += stdError;
                series.add(cnt2++, stdError);
            }
        }
        final XYSeriesCollection data = new XYSeriesCollection(series);
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "NCDG@20 Standard Error: " + methodName,
            "Queries", 
            "Standard Error", 
            data,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(WIDTH, HEIGHT));
        stdErrorTotal /= nQueries;
        meanTotal /= nQueries;
        DecimalFormat df = new DecimalFormat("#.#");
        String tmp = df.format(stdErrorTotal);
        double value = Double.parseDouble(tmp);
        ValueMarker marker = new ValueMarker(value);  
        marker.setPaint(Color.black);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.addRangeMarker(marker);
        // Draw png
        BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = bi.getGraphics();
        chartPanel.setBounds(0, 0, WIDTH, HEIGHT);
        chartPanel.paint(graphics);
        ImageIO.write(bi, "png", new File("plots/NCDG@20-Plot-Graph-" + methodName + ".png"));
        System.out.println("\tNCDG@20 mean: " + meanTotal);
        System.out.println("\tNCDG@20 standard Error: " + stdErrorTotal);
    }
    
    
    public ArrayList<QueryResult> getQueryStdErrs(String pageFilePath) throws IOException {
        ArrayList<QueryResult> ret = new ArrayList<QueryResult>();
        final int nQueries = getQueries(pageFilePath).size();
        int queryCnt = 0;
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            TopDocs topDocs = query(keywordQuery, MAX_REL_DOCS);
            double total = 0.0;
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                total += sd.score;
            }
            double nDocs = topDocs.scoreDocs.length;
            if(nDocs == 0)
                ret.add(new QueryResult(queryId, ++queryCnt, 0.0, 0.0));
            else {
                double mean = total / nDocs;
                total = 0.0;
                double deviation = 0.0;
                for (ScoreDoc sd : topDocs.scoreDocs) { 
                    Document doc = searcher.doc(sd.doc);
                    String paraId = doc.get("id");
                    double score = sd.score;
                    deviation = Math.abs(score - mean);
                    if(score > mean) 
                        deviation = -deviation;
                    deviation *= deviation;
                    total += deviation;
                }
                if(nDocs == 1) 
                    ret.add(new QueryResult(queryId, ++queryCnt, mean, 0));
                else {
                    double tmp = (double)total / (nDocs-1);
                    double stdDeviation = Math.sqrt(tmp);
                    double stdError = (double)stdDeviation / Math.sqrt(nDocs);
                    ret.add(new QueryResult(queryId, ++queryCnt, mean, stdError));
                }
            }
        }
        return ret;
    }
    
    
    public class QueryResult {
        
        private String queryId;
        private double mean;
        private double stdError;
        private double lowerVal, upperVal;
        private int queryNum = 0;
        
        public QueryResult(String queryId, int queryNum, double mean, double stdError) {
            this.queryId = queryId;
            this.mean = mean;
            this.stdError = stdError;
            this.lowerVal = mean - stdError;
            this.upperVal = mean + stdError;
            this.queryNum = queryNum;
        }
        
        public String toString() {
            String ret = "";
            ret = "--Query Result: (" + this.queryNum + ") " + this.queryId + "\n";
            ret += "\tMean: " + this.mean + "\n";
            ret += "\tStandard Error: " + this.stdError + "\n";
            ret += "\t\t" + this.lowerVal + ", " + this.upperVal;
            return ret;
        }
        
    }
    
    
    /**
     * Function: makeRunFile
     * Desc: Retrieve page name queries and create the run files.
     * @param cborLoc: The path where the page cbor file exists.
     */
    public void makeRunFile(String cborLoc) throws IOException {
        final int topN = 100; // get the top 100 results
        String outFileName = "run_files/" + methodName + "-runfile.txt";
        PrintWriter writer = new PrintWriter(outFileName, "UTF-8");
        // iterate through each page
        for (Data.Page p : SearchUtils.createPageIterator(cborLoc)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            TopDocs topDocs = query(keywordQuery, topN);
            int rank = topDocs.scoreDocs.length;
            // iterate through n (<= 100) top docs and output to run file  
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String paraId = doc.get("id");
                String score = Float.toString(sd.score);
                String out = queryId + " Q0 " + paraId + " " + rank--
                             + " " + score + " group2-" + methodName;		 
                writer.println(out);
            }
        }
        writer.close();
    }
    
    /**
     * Function: getQueries
     * Desc: Get an ArrayList of the queries set.
     * @param pageFilePath: The file containing the queries.
     * @param return: The queries set (strings).
     */
    public ArrayList<String>getQueries(String pageFilePath) {
        ArrayList<String> ret = new ArrayList<String>();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String keywordQuery = p.getPageName();
            ret.add(keywordQuery);
        }
        return ret;
    }
       
}
