/**
 * Main.java
 * 
 * UNH CS753
 * Progrmming Assignment 2
 * Group 2
 * 
 * This is the Main file for our second programming assignment. It first
 * creates the run files for part 1, a python script then runs trec_eval,
 * then this program runs parts 3 - 5. These steps run evaluation algorithms
 * for RPrecision, mean (map) and ncdg@20 measures. The results are
 * then compared to the trec_eval results. There is a script that runs
 * all of these steps and can be run with the command bash prog2.sh or
 * they can be entered in manually. (refer to README)
 * 
**/



package edu.unh.cs753;


import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Set;
import java.util.Scanner;
import edu.unh.cs753.indexing.LuceneSearcher;
import edu.unh.cs753.indexing.LuceneSearcher.QueryResult;
import edu.unh.cs753.indexing.LuceneIndexer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;
import org.apache.commons.io.IOUtils;



public class Main {
    
    static String paraFilePath; // = "cbor_files/train.pages.cbor-paragraphs.cbor";
    static String pageFilePath;  //= "cbor_files/train.pages.cbor-outlines.cbor";
    static String qrelFilePath; // = "cbor_files/train.pages.cbor-article.qrels";
    final static String indexPath = "paragraphs";
    static double rPrecDef = 0, rPrecCustom = 0;
    static double mapDef = 0, mapCustom = 0;
    static double ndcg20Def = 0, ndcg20Custom = 0;
    
    /**
     * Function: main
     * Desc: facilitate the trec_eval and evaluation algorithms.
     * @param args: program args (only 1 to run which part)
     */
    public static void main(String[] args) throws IOException, Exception {
        
        System.setProperty("file.encoding", "UTF-8");
        // must have arg1 be which part of the program to run
        
        String action; 
        try {
            paraFilePath = args[0];
            pageFilePath = args[1];
            qrelFilePath = args[2];
            action = args[3];
        }
        catch(ArrayIndexOutOfBoundsException iob) {
            System.out.println("Error: not enough args provided");
            throw iob; 
        }
        if(action.equals("part1")) {
            // run the indexing mechanism (only once per project run)
            System.out.println("Creating index...");
            LuceneIndexer indexer = new LuceneIndexer(indexPath);
            indexer.doIndex(paraFilePath);
            // make the run files necessary for step 2
            doPart1();
            System.out.println("Trec_eval results:");
            displayTrecEvalResults();
        }
        else if(action.equals("parts3-5")) {
            // Compute the results required and compare the results 
            // to the trec_eval results
            setTrecEvalVars();
            doPart3();
            doPart4();
            doPart5();
            doAnalysis();
        }
        else {
            System.err.println("Invalid argument: " + action);
            throw new Exception();
        }
    }
    
    /**
     * Function: doPart1
     * Desc: Create the run files.
     */
    private static void doPart1() throws IOException {
        // create run files (part 1)
        System.out.println("Creating run files...");
        // use the searcher to make runfiles
        LuceneSearcher searcher = new LuceneSearcher(indexPath);
        searcher.makeRunFile(pageFilePath); // default ranking function 
        searcher.custom(); // custom scoring function
        searcher.makeRunFile(pageFilePath);
    }
    
    /**
     * Function: doPart3
     * @param rPrecVals: trec_eval rPrec results
     * Desc: Find R precision results and compare to trec_eval.
     */
    private static void doPart3() throws IOException {
        System.out.println("--Part3--");
        System.out.println("Using the searcher to find R-Precision (compared to qrels file)...");
        LuceneSearcher searcher = new LuceneSearcher(indexPath);
        double rPrec = searcher.getRPrec(paraFilePath, pageFilePath, qrelFilePath);
        System.out.println("\tRprec: " + rPrec + ": " + "default");
        displayTrecComparison(rPrecDef, rPrec, "Rprec");
        searcher.custom();
        double rPrec2 = searcher.getRPrec(paraFilePath, pageFilePath, qrelFilePath);
        System.out.println("\tRprec: " + rPrec2 + ": " + "custom");
        displayTrecComparison(rPrecCustom, rPrec2, "Rprec");
        
    }
    
    /**
     * Function: doPart4
     * @param meanVals: trec_eval mean results
     * Desc: Find mean (map) results and compare to trec_eval.
     */
    private static void doPart4() throws IOException {
        System.out.println("--Part4--");
        System.out.println("Using the searcher to find mean (compared to qrels file)...");
        LuceneSearcher searcher = new LuceneSearcher(indexPath);
        double mean = searcher.getMeanPrec(paraFilePath, pageFilePath, qrelFilePath);
        System.out.println("\tMean Avg Prec: " + mean + ": " + "default");
        displayTrecComparison(mapDef, mean, "Mean");
        searcher.custom();
        mean = searcher.getMeanPrec(paraFilePath, pageFilePath, qrelFilePath);
        System.out.println("\tMean Avg Prec: " + mean + ": " + "custom");
        displayTrecComparison(mapCustom, mean, "Mean");
        
    }
    
    /**
     * Function: doPart5
     * @param ndcg20Vals: trec_eval ncdg@20 results
     * Desc: Find ncdg@20 results and compare to trec_eval.
     */
    private static void doPart5() throws IOException {
        System.out.println("--Part5--");
        System.out.println("Using the searcher to find ndcg@20 (compared to qrels file)...");
        LuceneSearcher searcher = new LuceneSearcher(indexPath);
        double ndcg20 = searcher.getNdcg20(paraFilePath, pageFilePath, qrelFilePath);
        System.out.println("\tNdcg@20: " + ndcg20 + ": " + "default");
        displayTrecComparison(ndcg20Def, ndcg20, "Ndcg@20");
        searcher.custom();
        ndcg20 = searcher.getNdcg20(paraFilePath, pageFilePath, qrelFilePath);
        System.out.println("\tNdcg@20: " + ndcg20 + ": " + "custom");
        displayTrecComparison(ndcg20Custom, ndcg20, "Ndcg@20");
        
    }
    
    
    private static void doAnalysis() throws IOException {
        
        System.out.println("--Part6--");
        System.out.println("Analysis of findings...");
        
        String outFileName = "query-arith-mean-and-stderr.txt";
        PrintWriter writer = new PrintWriter(outFileName, "UTF-8");
        
        LuceneSearcher searcher = new LuceneSearcher(indexPath);
    
        ArrayList<LuceneSearcher.QueryResult> results = searcher.getQueryStdErrs(pageFilePath);
        System.out.println("Writing to query mean and standard error results file");
        writer.println("Query Arithmetic mean and standard error results");
        writer.println("------Default scoring function-----");
        for(LuceneSearcher.QueryResult result : results) 
            writer.println(result);
            
        searcher.custom();
        results = searcher.getQueryStdErrs(pageFilePath);
        writer.println("\n------Custom scoring function-----");
        for(LuceneSearcher.QueryResult result : results) 
            writer.println(result);
        writer.close();
        
        searcher = new LuceneSearcher(indexPath);
        System.out.println("Plotting R-Precision Default scoring function standard error");
        searcher.plotRPrecStdErr(paraFilePath, pageFilePath, qrelFilePath);
        searcher.custom();

        System.out.println("Plotting R-Precision Custom scoring function standard error");
        searcher.plotRPrecStdErr(paraFilePath, pageFilePath, qrelFilePath);
        
        searcher = new LuceneSearcher(indexPath);
        System.out.println("Plotting Map Default scoring function standard error");
        searcher.plotMAPStdErr(paraFilePath, pageFilePath, qrelFilePath);
        searcher.custom();
        System.out.println("Plotting Map Custom scoring function standard error");
        searcher.plotMAPStdErr(paraFilePath, pageFilePath, qrelFilePath);
        
        searcher = new LuceneSearcher(indexPath);
        System.out.println("Plotting NCDG@20 Default scoring function standard error");
        searcher.plotNCDG20stdErr(paraFilePath, pageFilePath, qrelFilePath);
        searcher.custom();
        System.out.println("Plotting NCDG@20 Custom scoring function standard error");
        searcher.plotNCDG20stdErr(paraFilePath, pageFilePath, qrelFilePath);
        
    }
    
    
    /**
     * Function: displayTrecComparison
     * Desc: Display a result compared to the trec_eval result.
     * @param trecEvalResult: The trec_eval result (average among queries) to compare to.
     * @param result: The result (average among queries) from searcher.
     * @param which: Which result (for display).
     */
    private static void displayTrecComparison(double trecEvalResult,
                                       double result, String which) {
        double dif;
        if(result < trecEvalResult) {
            dif = trecEvalResult - result;
            System.out.println("\t\tNOTE: " + which + 
            " result scored lower than trec_eval result by: " + dif);
        } 
        else if(result > trecEvalResult) {
            dif = result - trecEvalResult;
            System.out.println("\t\tNOTE: " + which + 
            " result scored higher than trec_eval result by: " + dif);
        } 
        else {
            System.out.println("\t\tNOTE: " + which + 
            " result equalled trec_eval result");
        }
    }
    
    /**
     * Function: getTrecEvalResults
     * Desc: this is a crude mechanism for comparing results to the
     * pytec_eval results generated from the python scriptfrom part2.
     */
    private static void setTrecEvalVars() throws IOException {
        ArrayList<Double> ret = new ArrayList<Double>();
        try (BufferedReader br = new BufferedReader(new FileReader("trec_eval_results.txt"))) {
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                Scanner scanner = new Scanner(line.trim());
                while (scanner.hasNext()) {
                    String token = scanner.next();
                    Double d;
                    try {
                        d = Double.parseDouble(token);
                        if(i < 3) {
                            if(i == 0)
                                rPrecDef = d;
                            else if(i == 1)
                                mapDef = d;
                            else
                                ndcg20Def = d;
                        } 
                        else {
                            if(i == 3)
                                rPrecCustom = d;
                            else if(i == 4)
                                mapCustom = d;
                            else
                                ndcg20Custom = d; 
                        }
                        i++;
                    } catch(NumberFormatException nfe) {}
                    
                }
                scanner.close();
            }
        }
    }
    
    
    private static void displayTrecEvalResults() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("trec_eval_results.txt"))) {
            String line;
            while ((line = br.readLine()) != null) 
                System.out.println(line);
        }
    }
    
}
