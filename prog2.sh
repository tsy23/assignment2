#!/bin/bash
#
# CS873 Programming Assignment2 main script Team2
#
# This script runs the necessary steps for parts 1 and 2 of programming 
# assignment 2. 
# 
# 1) maven compile
# 2) run the maven jar file for part 1
# 3) run the python script that uses pytrec_eval to get results for part 2
# 4) run the maven jar file for part 3
# 5) run the maven jar file for part 5
# 6) run the maven jar file for part 6
#
#



if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters (must be 3)"
    exit 1
fi



# remove program created files and directories (if they exist)
rm -rf ./run_files
rm -rf ./paragraphs
rm -rf ./plots
# don't do this, in case they don't have pytrec_eval installed
#(leave the results file)
#rm -rf trec_eval_results.txt
rm -rf query-arith-mean-and-stderr.txt

# maven compile command
mvn clean compile assembly:single
if [ $? -eq 1 ]; then
    echo "Maven compile failed"
    exit 1
fi

# create a run_files directory
mkdir ./run_files
mkdir ./plots

#System.out.println("\n\n===Programming Assignment 2 Team 2===");
printf "\n\n===Programming Assignment 2 Team 2===\n"


# run the maven jar file (for part 1)
echo "--part1 (creating index and making run files)"
java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 part1
if [ $? -eq 1 ]; then
    echo "Error running jar file (part1)"
    exit 1
fi

# run the python script that uses pytrec_eval to get results (for part 2)
#echo "--part2 (using trec_eval)"
#python3 part2.py
#if [ $? -eq 1 ]; then
    #echo "Error running pytrec_eval script (part2)"
    # refer to note above
    echo "Using existing trec_eval results file"
    #exit 1
#fi

# run the maven jar file (for part 3-5)
echo "--parts3-5 (Rprec, mean and ndcg@20 results)"
java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 parts3-5
if [ $? -eq 1 ]; then
    echo "Error running jar file (parts3-5)"
    exit 1
fi

echo "====================================="
